/**
 * @file openxum/core/games/sixth/move.cpp
 * See the AUTHORS or Authors.txt file
 */

/*
 * Copyright (C) 2011-2019 Openxum Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <openxum/core/games/sixth/move.hpp>

using namespace std;

namespace openxum {
    namespace core {
        namespace games {
            namespace sixth {
                Move(const MoveType::Values& ty, const coordinates& f, const Coordinates& to, int p);

                Move::Move(const MoveType::Values& ty, const coordinates& f, const Coordinates& to, int p)
                        :_type(ty),_from(f), _to(to), _piece(p) { }

                openxum::core::common::Move* Move::clone() const
                {
                    return new Move(_type, _from, _to, _piece);
                }

                int Move::get_piece()
                {
                    return _piece;
                }

                coordinates Move::get_from() {
                    return _from;
                }

                coordinates Move::get_to() {
                    return _to;
                }

                void Move::decode(const string& str)
                {
                      if (str[0] == 'P') {
                           _type = MoveType::PUT_PIECE;
                           _from = Coordinates(-1,-1);
                           _to   = Coordinates(str[1] - 'a', str[2] - '1');
                           _piece = atoi(str[5].c_str());
                       }
                       else {
                            (str.size() == 4) {
                                  _type = MoveType.MOVE_TOWER;
                                  _from = Coordinates(str[2] - 'A', str[3] - '1');
                                  _to = Coordinates(str[3] - 'a', str[4] - '1');
                                  _piece = atoi(str[5].c_str());
                                }
                            }
                    }
                }

                std::string Move::encode() const
                {
                    if (_type == MoveType.PUT_PIECE) {
                                string encode = "P " + _to.to_string();
                                return encode;
                            }
                            else {
                                string encode = "M " + _from.to_string() + _to.to_string() + get_piece();
                                return encode;
                            }
                }

                void Move::from_object(const Move data)
                {
                    this->_type = data._type;
                    this->_from = data._from;
                    this->_to   = data._to;
                    this->_piece= data._piece;
                }

                nlohmann::json Move::to_object() const
                {
                    nlohmann::json json;

                    json["type"] = _type;
                    json["from"] = _from.to_object();
                    json["to"]   = _to.to_object();
                    json["piece"] = _piece;
                    return json;
                }

                std::string Move::to_string() const
                {
                     if (_type == MoveType::PUT_PIECE) {
                        return "PUT " + std::string(_color == Color::BLACK ? "black" : "white") + " PIECE at " +
                                _to.to_string();
                    } else if (_type == MoveType::MOVE_TOWER) {
                        return "MOVE " + std::string(_color == Color::BLACK ? "black" : "white") + " PIECE at " +
                                _to.to_string();
                    }
                    return "";
                }

            }
        }
    }
}