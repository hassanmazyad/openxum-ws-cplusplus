/**
 * @file openxum/core/games/sixth/move.hpp
 * See the AUTHORS or Authors.txt file
 */

/*
 * Copyright (C) 2011-2019 Openxum Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPENXUM_CORE_GAMES_SIXTH_MOVE_HPP
#define OPENXUM_CORE_GAMES_SIXTH_MOVE_HPP

#include <openxum/core/common/move.hpp>
#include <openxum/core/games/sixth/coordinates.hpp>
#include <openxum/core/games/sixth/move_type.hpp>
#include "../../common/json.hpp"

namespace openxum {
    namespace core {
        namespace games {
            namespace sixth {

                class Move : public openxum::core::common::Move {

                private:
                    MoveType::Values _type;
                    Coordinates _from;
                    Coordinates _to;
                    int _piece;

                public:
                    Move(const MoveType::Values& ty, const coordinates& f, const Coordinates& to, int p);

                    openxum::core::common::Move* Move::clone() const;

                    void decode(const string& str) override;

                    std::string encode() const override;

                    void from_object(const nlohmann::json&) override;

                    MoveType::Values get_type() const { return _type; }

                    coordinates get_from() ;

                    coordinates get_to();

                    int get_piece();

                    bool is_equal( openxum::core::common::Move move2);

                    void to_object();

                    std::string to_string() const override;
                };

            }
        }
    }
}

#endif