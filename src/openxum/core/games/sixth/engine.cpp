/**
 * @file openxum/core/games/sixth/engine.cpp
 * See the AUTHORS or Authors.txt file
 */

/*
 * Copyright (C) 2011-2019 Openxum Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <openxum/core/games/sixth/engine.hpp>
#include <openxum/core/games/sixth/move.hpp>
#include <openxum/core/games/sixth/phase.hpp>
#include <openxum/core/games/sixth/color.hpp>


namespace openxum {
    namespace core {
        namespace games {
            namespace sixth {

                Engine::Engine(){
                    this->_color=color::white;
                    this->_nb_black_piece=15;
                    this->_nb_white_piece=15;
                    this->_towers = vector<Coordinates>towers;
                    this->_move_to_avoid=null;
                    this->_phase = Phase::MOVE_TOWER;
                    this->_winner_color = Color::NONE;
                }

                Engine::~Engine()
                {
                    for (int i = 0; i < _towers.size(); i++) {
                        delete[] _towers[i];
                    }
                    delete[] _towers;
                }


                vector<coordinates> Engine::get_towers()
                {
                    return _towers;
                }

                int Engine::get_black_piece_remaining(){
                    return _nb_black_piece;
                }

                int Engine::get_white_piece_remaining()
                {
                    return _nb_white_piece;
                }
                bool Engine::is_finished() const{
                    return _phase == Phase::FINISH;
                }

                Engine* Engine::clone() const
                {
                    Engine e = new Engine();

                    e->_color =_color;

                    e->_nb_while_piece = _nb_white_piece;
                    e->_nb_black_piece = _nb_black_piece;

                    nTowers = vector<coordinates>nTowers[_towers.size()];

                    for (int i = 0; i < nTowers.size();i++) {
                        nPieces = vector<int>nPieces[_towers.pieces.size()];

                        for (int j = 0 ; j<nPieces.size() ; j++){
                            nPieces = _towers[i].pieces[j];
                        }

                        nTowers[i]._x = _towers[i]._x;
                        nTowers[i]._y = _towers[i]._y;
                    }

                    e->_towers = nTowers;
                    e->_move_to_avoid = _move_to_avoid;
                    e->_phase = _phase;
                    return e;
                }

                void Engine::_next_color(){
                    _color = _color == Color::BLACK ? Color::WHITE : Color::BLACK;
                }


                const string& Engine::get_name();
                {
                    return "SIXTH";
                }

                Color Engine::winner_is(){
                    return _winner_color;
                }

                int Engine::_search_tower(int x, int y){
                    for (int i = 0; i < _towers.size(); i++) {
                           if (_towers[i]._x == x && _towers[i]._y == y) {
                               return _towers[i];
                        }
                    }
                    return null;
                }

                int Engine::_search_tower_index(int x,int  y){
                    for (int i = 0; i < _towers.size(); i++) {
                            if (_towers[i]._x == x && _towers[i]._y == y) {
                                return i;
                            }
                     }
                     return null;
                }




                void Engine::_process_move(openxum::core::common::Move* move, int index_tower_from, int index_tower_to){

                    Coordinates to_move = _towers[index_tower_from].pieces.slice(move.get_piece());

                            _towers[index_tower_to].pieces = _towers[index_tower_to].pieces.concat(to_move);

                            const int tower_length_to = _towers[index_tower_to].pieces.length;

                            if (tower_length_to >= 6) {
                                _phase = Phase::FINISH;
                                _winner_color = _towers[index_tower_to].pieces[tower_length_to - 1];
                            }

                            if (move.get_piece() == 0) {
                                _remove_tower(index_tower_from);
                            } else {
                                _towers[index_tower_from].pieces.pop_back(move.get_piece());
                            }
                }

                void Engine::_remove_tower(int index_tower_to_remove) {
                     _towers.pop_back(index_tower_to_remove, 1);
                }


                bool Engine::verify_move(openxum::core::common::Move* move){
                            const list = this.get_possible_move_list();
                            for (int i = 0; i < list.size(); i++) {
                                if (move.is_equal(list[i])) {
                                    return true;
                                }
                            }
                            return false;
                }

                vector<coordinates> Engine::_get_possible_putting_list() {
                        coordinates point = new coordinates(-1,-1)
                        vector<coordinates>putting_list;
                        for (int i = 0; i < 5; i++) {
                            for (int j = 0; j < 5; j++) {
                                if (!this._search_tower(i, j)) {
                                    putting_list.push_back(Move(MoveType.PUT_PIECE);
                                }
                            }
                        }
                        return putting_list;
                    }


                     vector<coordinates> Engine::_get_possible_moves_in_direction(coordinates start_tower, int DirectionX, int DirectionY);

                            vector<coordinates> move_list[0]
                            if (start_tower._x + DirectionX >= 0 && start_tower._x + DirectionX < 5 && (start_tower._y + DirectionY >= 0 && start_tower._y + DirectionY < 5) ){
                                if (this._search_tower(start_tower._x + DirectionX, start_tower._y + DirectionY)) {
                                    coordinates dest_tower = this._search_tower(start_tower._x + DirectionX, start_tower._y + DirectionY);
                                    if (dest_tower != null) {
                                        move_list.push_back(this._get_moves_between_towers(start_tower, dest_tower));
                                    }
                                }
                            }
                            return move_list;
                        }

                    vector<coordinates> Engine::_get_moves_between_towers(coordinates start_tower, coordinates dest_tower){
                            vector<coordinates>moves_between_towers[0];
                            for (int j = 0; j < dest_tower.size(); j++) {
                                const move = new Move(MoveType.MOVE_TOWER, coordinates(start_tower._x,start_tower._y) {
                                    x: dest_tower.x,
                                    y: dest_tower.y
                                }, j);
                                if (!this._move_to_avoid || !move.is_equal(this._move_to_avoid)) {
                                    moves_between_towers.push_back(move);
                                }
                            }
                            return moves_between_towers;

                        }

                    vector<coordinates> _get_possible_moves_to_tower_list(coordinates start_tower) {
                        vector<coordinates>move_list[0];
                        int j = this._search_tower_index(start_tower._x, start_tower._y);
                        if (this._towers[j].pieces.size()==1) {
                            for (int DirectionX = -1; DirectionX <= 1; DirectionX++) {
                                for (int DirectionY = -1; DirectionY <= 1; DirectionY++) {
                                    if ((DirectionX == 0 || DirectionY == 0) && !(DirectionX == 0 && DirectionY == 0)) {
                                        move_list.push_back(this._get_possible_moves_in_direction(start_tower, DirectionX, DirectionY));
                                    }
                                }
                            }
                        } else if (this._towers[j].pieces.size()==2) {
                            for (int DirectionX = -4; DirectionX <= 4; DirectionX++) {
                                for (int DirectionY = -4; DirectionY <= 4; DirectionY++) {
                                    if ((DirectionX == 0 || DirectionY == 0) && !(DirectionX == 0 && DirectionY == 0)) {
                                        move_list.push_back(this._get_possible_moves_in_direction(start_tower, DirectionX, DirectionY));
                                    }
                                }
                            }
                        } else if (this._towers[j].pieces.size()==3) {
                            for (let DirectionX = -4; DirectionX <= 4; DirectionX++) {
                                for (let DirectionY = -4; DirectionY <= 4; DirectionY++) {
                                    if ((DirectionX == 2 && DirectionY == -1) || (DirectionX == -1 && DirectionY === -2) || (DirectionX == 1 && DirectionY == -2) || (DirectionX == -1 && DirectionY == 2) || (DirectionX == 2 && DirectionY == 1) || (DirectionX == -2 && DirectionY == -1) || (DirectionX == -2 && DirectionY == 1) || (DirectionX == 1  && DirectionY == 2 ) )
                                        move_list.push_back(this._get_possible_moves_in_direction(start_tower, DirectionX, DirectionY));
                                    }
                                }
                            }
                        else if (this._towers[j].pieces.size()==4 {
                            for (let DirectionX = -4; DirectionX <= 4; DirectionX++) {
                                for (let DirectionY = -4; DirectionY <= 4; DirectionY++) {
                                    if (((DirectionX == DirectionY) || (DirectionX == -DirectionY) ) && ((DirectionX !=0 ) && (DirectionY !=0 ))) {
                                        move_list.push_back(this._get_possible_moves_in_direction(start_tower, DirectionX, DirectionY));
                                    }
                                }
                            }
                        }else if (this._towers[j].pieces.size()==5 {
                            for (let DirectionX = -4; DirectionX <= 4; DirectionX++) {
                                for (let DirectionY = -4; DirectionY <= 4; DirectionY++) {
                                    if (((((DirectionX == DirectionY) || (DirectionX == -DirectionY) ) && ((DirectionX !=0 ) && (DirectionY !==0 ))) || (DirectionX == 0 || DirectionY == 0) && !(DirectionX == 0 && DirectionY == 0))) {
                                       move_list.push_back(this._get_possible_moves_in_direction(start_tower, DirectionX, DirectionY));
                                    }
                                }
                            }
                        }
                        return move_list;
                    }


                    void Engine::_move_piece(openxum::core::common::Move* move) {
                            int index_tower_from = this._search_tower_index(move.get_from()._x, move.get_from()._y);
                            int index_tower_to = this._search_tower_index(move.get_to()._x, move.get_to()._y);
                            this._move_to_avoid = new Move(move.get_type(), move.get_to(), move.get_from(), this._towers[index_tower_to].pieces.size());
                            this._move_to_avoid = null;
                            this._process_move(move, index_tower_from, index_tower_to);

                        }

                    void Engine::_put_piece(openxum::core::common::Move* move) {
                            Coordinates new_tower = Coordinates(move.get_to()._x ,  move.get_to()._y);
                            _towers = _towers.push_back(new_tower);
                            _remove_piece_from_reserve();
                            _move_to_avoid = null;
                    }


                    void Engine::move(const openxum::core::common::Move* move){

                        if (move.get_type() == MoveType::PUT_PIECE) {
                                    this._put_piece(move);
                                } else {
                                    this._move_piece(move);
                                }

                                this._next_color();

                                if (this.get_possible_move_list.size() == 0) {
                                    this._phase = Phase::FINISH;
                                }

                    }



            }
        }