/**
 * @file openxum/core/games/sixth/engine.hpp
 * See the AUTHORS or Authors.txt file
 */

/*
 * Copyright (C) 2011-2019 Openxum Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef OPENXUM_CORE_GAMES_SIXTH_ENGINE_HPP
#define OPENXUM_CORE_GAMES_SIXTH_ENGINE_HPP

#include <openxum/core/common/engine.hpp>

#include <openxum/core/games/sixth/move.hpp>
#include <openxum/core/games/sixth/phase.hpp>
#include <openxum/core/games/sixth/coordinates>
#include <openxum/core/games/sixth/color>
#include <vector>

using namespace std;


namespace openxum {
    namespace core {
        namespace games {
            namespace sixth {

                class Engine : public openxum::core::common::Engine {

                private:
                    Color _color;
                    int _nb_black_piece;
                    int _nb_white_piece;
                    vector<coordinates>_towers ;
                    Move _move_to_avoid;
                    Phase _phase;
                    Color _winner_color;

                public:
                    Engine() = default;

                    ~Engine();

                    openxum::core::common::Move* build_move() const override { return new Move(); }

                    Engine* clone() const override;

                    Color current_color() const override
                    { return _color; }

                    String current_color_string() const override
                    { return _color == Color.BLACK ? "Black" : "White" };

                    const std::string& get_name();

                    bool is_finished() const {};

                    void move(const openxum::core::common::Move* move);

                    void parse(const std::string&) override { }

                    std::string to_string() const override;

                    int winner_is();

                    void change_color()
                    {
                        _color = _color == Color::BLACK ? Color::WHITE : Color::BLACK;
                    }

                    vector<coordinates> get_towers();

                    int get_black_piece_remaining();

                    int get_white_piece_remaining();

                    bool verify_move(openxum::core::common::Move* move);

                    vector<coordinates> get_possible_move_list();

                    int _search_tower(int x, int y);

                    int _search_tower_index(int x,int  y);

                    void _next_color();

                    void _put_piece(openxum::core::common::Move* move);

                    void _process_move(openxum::core::common::Move* move, int index_tower_from, int index_tower_to);

                    void _remove_tower(int index_tower_to_remove);

                    vector<coordinates>_get_possible_putting_list();

                    vector<coordinates> _get_possible_moves_to_tower_list(coordinates start_tower);

                    vector<coordinates> _get_moves_between_towers(coordinates start_tower, coordinates dest_tower) ;

                    vector<coordinates> _get_possible_moves_in_direction(coordinates start_tower, int DirectionX, int DirectionY);



                };

            }
        }
    }
}

#endif